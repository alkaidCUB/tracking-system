# frozen_string_literal: true

require './test/test_helper'

class TestShipmentServicesStrategy < Test
  def test_unimplement_required_keys_method
    assert_raises NotImplementedError do
      UnimplementedRequiredKeysStrategy.new({})
    end
  end

  def test_unimplement_track_shipment_method
    assert_raises NotImplementedError do
      strategy = UnimplementedTrackShipment.new({})
      strategy.track_shipment
    end
  end

  class UnimplementedRequiredKeysStrategy < ShipmentServicesStrategy::ShipmentServiceAbstractStrategy
  end

  class UnimplementedTrackShipment < ShipmentServicesStrategy::ShipmentServiceAbstractStrategy
    def required_keys
      []
    end
  end
end
