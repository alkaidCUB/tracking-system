# frozen_string_literal: true

require './test/test_helper'

class TestTracking < TestRoute
  def setup
    @tracking_number = '1276371286'
  end

  def test_tracking_successful
    ShipmentServicesStrategy::FedexStrategy.any_instance.expects(:track_shipment).yields

    post 'api/v1/tracks', new_track_data

    track_id = last_response_json['data']['track_id']

    post "api/v1/tracks/#{track_id}"
  end

  def test_fedex_process_call
    mock_result = mock('fedex result')
    mock_tracking_info = mock('tracking info')
    mock_events = mock('events')
    mock_event = mock('event')

    mock_tracking_info.expects(:status).returns('At Pickup')
    mock_tracking_info.expects(:events).returns(mock_events)
    mock_events.expects(:first).returns(mock_event)
    mock_event.expects(:description).returns('In FedEx possession')

    mock_result.expects(:first).returns(mock_tracking_info)

    Services[:fedex].stubs(:track).returns(mock_result)

    stub_request(:post, 'http://example.com/')
      .with(
        body: { status: 'CREATED', description: 'In FedEx possession', track_id: 'some_track_id' },
        headers: {
          'Accept' => '*/*',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/json',
          'User-Agent' => 'Ruby'
        }
      )
      .to_return(status: 200, body: '', headers: {})

    data = { track_id: 'some_track_id', tracking_number: '127631286', notification_url: 'http://example.com' }
    fedex_strategy = ShipmentServicesStrategy::Creator.for('FEDEX').new(data)
    fedex_strategy.process_call
  end

  def test_fedex_notify_expired_tracking_number
    # mock_result = mock('fedex result')
    # mock_tracking_info = mock('tracking info')
    # mock_events = mock('events')
    # mock_event = mock('event')
    #
    # mock_tracking_info.expects(:status).returns('At Pickup')
    # mock_tracking_info.expects(:events).returns(mock_events)
    # mock_events.expects(:first).returns(mock_event)
    # mock_event.expects(:description).returns('In FedEx possession')
    #
    # mock_result.expects(:first).returns(mock_tracking_info)
    #
    #
    # Services[:fedex].stubs(:track).returns(mock_result)
    #
    # mock_client = mock('notification client')
    #
    # expected_options = {
    #     headers: { "Content-Type": "application/json" },
    #     :body => {"status":"CREATED","description":"In FedEx possession"}.to_json
    # }
    #
    # mock_httparty = mock('httparty')
    #
    # HTTParty.expects(:post).once.with('http://example.com', expected_options).returns(mock_httparty)
    #
    # mock_httparty.expects(:code).returns(200)
    #

    stub_request(:post, 'http://example.com/')
      .with(
        body: {
          tracking_number: '127637286',
          status: 'This tracking number cannot be found or is expired',
          track_id: 'some_track_id'
        },
        headers: {
          'Accept' => '*/*',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/json',
          'User-Agent' => 'Ruby'
        }
      )
      .to_return(status: 200, body: '', headers: {})

    data = { track_id: 'some_track_id', tracking_number: '127637286', notification_url: 'http://example.com' }
    fedex_strategy = ShipmentServicesStrategy::Creator.for('FEDEX').new(data)
    fedex_strategy.notify_expired_tracking_number
  end

  private

  def new_track_data
    {
      shipment_service: 'FEDEX',
      data: { tracking_number: @tracking_number }.to_json,
      notification_url: 'http://some.com'
    }
  end
end
