# frozen_string_literal: true

require './test/test_helper'

class TestNotifications < TestRoute
  def test_notifications_endpoint
    post 'api/v1/notification'
  end

  def test_notifications_client_invalid_response
    stub_request(:post, 'http://example.com/')
      .with(
        body: { track_id: 'test_track_id' }.to_json,
        headers: {
          'Accept' => '*/*',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/json',
          'User-Agent' => 'Ruby'
        }
      )
      .to_return(status: 400, body: '', headers: {})

    NotificationClient.new('http://example.com', 'test_track_id', {}).notify
  end
end
