# frozen_string_literal: true

require './test/test_helper'

class TestAddTrack < TestRoute
  def test_add_new_track
    post 'api/v1/tracks', new_track_data

    assert_equal 'success', last_response_json['status']
    assert_equal 'OK', last_response_json['message']
  end

  def test_add_new_track_with_missing_keys
    track_data = new_track_data
    track_data.except!(:data)
    track_data[:data] = { something: 'something' }.to_json

    exception = assert_raises Exceptions::MissingKeysException do
      post 'api/v1/tracks', track_data
    end

    assert_equal '[:tracking_number]', exception.message
  end

  def test_add_new_track_missing_parameters
    post 'api/v1/tracks', {}

    error_expected = 'shipment_service is missing, shipment_service is empty, ' \
                     'data is missing, data is empty, ' \
                     'notification_url is missing, notification_url is empty'

    assert_equal error_expected, last_response_json['error']
  end

  private

  def new_track_data
    {
      shipment_service: 'FEDEX',
      data: { tracking_number: '1276371286' }.to_json,
      notification_url: 'http://some.com'
    }
  end
end
