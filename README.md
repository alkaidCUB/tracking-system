# Tracking system

### Setup api -> ruby version 2.6.6

1.- Git clone repo.

2.- Execute

```sh
bundle install
```

3.- Setup files api/config/ with required keys

4.- Run db migrations

```sh
sequel -m api/database/migrations postgres://postgres@localhost:5432/tracking
```

5.- Run server

```sh
bundle exec rackup -p 8080
```

6.- Run test cases

```sh
bundle exec rake test
```