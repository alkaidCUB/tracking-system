# frozen_string_literal: true

module Constants
  module TrackingStates
    CREATED = 'CREATED'
    ON_TRANSIT = 'ON_TRANSIT'
    DELIVERED = 'DELIVERED'
    EXCEPTION = 'EXCEPTION'
  end

  module RequestsStatus
    SUCCESS = 'success'
    EXPIRED = 'expired'
    FAILED = 'failed'
  end
end
