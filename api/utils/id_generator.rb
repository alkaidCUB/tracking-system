# frozen_string_literal: true

require 'uuidtools'

class UUIDGenerator
  include Singleton

  def generate
    UUIDTools::UUID.timestamp_create.to_str.gsub('-', '')
  end
end
