# frozen_string_literal: true

require './api/exceptions/unexpected_response_from_notification_url'

class NotificationClient
  def initialize(url, track_id, data)
    @url = url
    @data = data
    @data[:track_id] = track_id
  end

  def notify
    options = {
      headers: {
        'Content-Type': 'application/json'
      },
      body: @data.to_json
    }
    response = HTTParty.post(@url, options)

    return if response.code / 100 == 2

    logger.info "Invalid response from notification url: track_id: #{@data[:track_id]}"
  end

  private

  def logger
    @logger ||= Logger.new(STDOUT)
  end
end
