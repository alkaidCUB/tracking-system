# frozen_string_literal: true

require_relative 'shipment_service_abstract_strategy'
require './api/utils/notification_client'
require './api/utils/constants'
require './api/exceptions/invalid_fedex_tracking_number'
require './api/dao/requests_dao'

module ShipmentServicesStrategy
  # Fedex shipment service strategy
  class FedexStrategy < ShipmentServiceAbstractStrategy
    include Constants
    def required_keys
      [:tracking_number]
    end

    # :nocov:
    def track_shipment
      Thread.new do
        max_requests.times do
          begin
            process_call
            break
          rescue StandardError => e
            if e.class.to_s == 'Fedex::RateError'
              notify_expired_tracking_number
              break
            end
            save_event_related_to_fedex_error
          end

          sleep delay
        end
      end
    end
    # :nocov:

    CREATED = 'CREATED'
    ON_TRANSIT = 'ON_TRANSIT'
    DELIVERED = 'DELIVERED'
    EXCEPTION = 'EXCEPTION'

    # I just choose status as I thought it could be
    def status(fedex_status)
      mapping = {
        'At Pickup' => TrackingStates::CREATED,
        'Shipment information sent to FedEx' => TrackingStates::CREATED,
        'Departed FedEx location' => TrackingStates::ON_TRANSIT,
        'On FedEx vehicle for delivery' => TrackingStates::ON_TRANSIT,
        'At destination sort facility' => TrackingStates::DELIVERED,
        'Departed Delivered location' => TrackingStates::DELIVERED,
        'At FedEx destination facility' => TrackingStates::DELIVERED
      }

      return TrackingStates::EXCEPTION unless mapping.key?(fedex_status)

      mapping[fedex_status]
    end

    def process_call
      logger.info "Start tracking to fedex: track_id: #{@data[:track_id]}"
      results = fedex.track(tracking_number: @data[:tracking_number])
      tracking_info = results.first
      fedex_status = tracking_info.status
      description = tracking_info.events.first.description

      data = {
        status: status(fedex_status),
        description: description
      }

      requests_dao.add(@data[:track_id], RequestsStatus::SUCCESS, data.to_json)

      logger.info "Shipment successfully tracked on fedex: track_id: #{@data[:track_id]}"
      notify(data)
    end

    def notify_expired_tracking_number
      logger.info "Tracking number not found on fedex or is expired: track_id: #{@data[:track_id]}"

      requests_dao.add(@data[:track_id], RequestsStatus::EXPIRED)

      data = {
        tracking_number: @data[:tracking_number],
        status: 'This tracking number cannot be found or is expired'
      }
      notify(data)
    end

    # :nocov:
    def save_event_related_to_fedex_error
      requests_dao.add(@data[:track_id], RequestsStatus::FAILED)
      logger.info "Error from fedex: Probably unavailable: track_id: #{@data[:track_id]}"
    end
    # :nocov:

    private

    def fedex
      Services[:fedex]
    end

    # :nocov:
    def delay
      ServiceConfig[:configuration][:fedex][:tracking][:delay]
    end

    def max_requests
      ServiceConfig[:configuration][:fedex][:tracking][:max_requests]
    end
    # :nocov:

    def notify(data)
      NotificationClient.new(@data[:notification_url], @data[:track_id], data).notify
    end

    def logger
      @logger ||= Logger.new(STDOUT)
    end

    def requests_dao
      DAO::RequestsDAO.instance
    end
  end
end
