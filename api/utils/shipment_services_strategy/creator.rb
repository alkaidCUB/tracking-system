# frozen_string_literal: true

require './api/exceptions/unsupported_shipment_service'
require_relative 'fedex_strategy'

module ShipmentServicesStrategy
  # Builder class for Shipment Services Strategy
  class Creator
    STRATEGIES = {
      'FEDEX' => FedexStrategy
    }.freeze

    def self.for(shipment_service)
      raise Exceptions::UnsupportedShipmentException unless STRATEGIES.key?(shipment_service.to_s)

      STRATEGIES[shipment_service]
    end
  end
end
