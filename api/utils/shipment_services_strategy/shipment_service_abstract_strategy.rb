# frozen_string_literal: true

require './api/exceptions/missing_keys'

module ShipmentServicesStrategy
  # Base Shipment Service class
  class ShipmentServiceAbstractStrategy
    def initialize(data)
      @data = data
      validate_data
    end

    def validate_data
      missing_keys = find_missing_keys(required_keys, @data)

      raise Exceptions::MissingKeysException, missing_keys unless missing_keys.empty?
    end

    def required_keys
      raise NotImplementedError
    end

    def track_shipment
      raise NotImplementedError
    end

    private

    def find_missing_keys(required, data)
      missing_keys = []
      required.each do |key|
        missing_keys << key unless data.key?(key)
      end
      missing_keys
    end
  end
end
