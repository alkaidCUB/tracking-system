# frozen_string_literal: true

Sequel.migration do
  transaction

  up do
    extension :pg_triggers

    create_table(:requests) do
      Serial   :request_id, primary_key: true
      String   :track_id, size: 50, null: false
      String   :status, size: 50, null: false
      Jsonb    :data

      DateTime :created_at, default: Sequel.function(:NOW)
      DateTime :updated_at, default: Sequel.function(:NOW)
    end

    pgt_updated_at(:requests,
                   :updated_at,
                   function_name: 'update_updated_at_column_at_requests_table',
                   trigger_name: :set_updated_at)
  end
end
