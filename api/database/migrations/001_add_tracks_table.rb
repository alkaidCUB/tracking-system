# frozen_string_literal: true

Sequel.migration do
  transaction

  up do
    extension :pg_triggers

    create_table(:tracks) do
      String   :id_track, size: 50, primary_key: true
      String   :shipment_service, size: 50, null: false
      Jsonb    :data
      String   :notification_url, size: 200, null: false

      DateTime :created_at, default: Sequel.function(:NOW)
      DateTime :updated_at, default: Sequel.function(:NOW)
    end

    pgt_updated_at(:tracks,
                   :updated_at,
                   function_name: 'update_updated_at_column_at_tracks_table',
                   trigger_name: :set_updated_at)
  end
end
