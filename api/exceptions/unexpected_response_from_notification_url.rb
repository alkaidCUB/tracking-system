# frozen_string_literal: true

module Exceptions
  class UnexpectedResponseFromNotificationUrlException < StandardError
  end
end
