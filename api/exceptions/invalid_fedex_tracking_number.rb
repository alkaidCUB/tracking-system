# frozen_string_literal: true

module Exceptions
  class InvalidFedexTrackingNumberException < StandardError
  end
end
