# frozen_string_literal: true

module Exceptions
  class UnsupportedShipmentException < StandardError
  end
end
