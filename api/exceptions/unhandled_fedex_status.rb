# frozen_string_literal: true

module Exceptions
  class UnhandledFedexStatusException < StandardError
  end
end
