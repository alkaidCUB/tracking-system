# frozen_string_literal: true

module Exceptions
  class InvalidTrackIdException < StandardError
  end
end
