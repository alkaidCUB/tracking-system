# frozen_string_literal: true

module Exceptions
  class MissingKeysException < StandardError
  end
end
