# frozen_string_literal: true

require './api/entities/tracks'
require './api/business/tracks'

module Routes
  module V1
    module Endpoints
      # Endpoints related to charges operations
      class Tracks < Grape::API
        resource :tracks do
          desc 'Add new track' do
            detail 'Add new track'
            success model: Entities::Tracks, examples: Entities::Tracks.add_tracks_example
          end
          params do
            requires :shipment_service,
                     type: String,
                     desc: 'External shipment service name',
                     allow_blank: false
            requires :data,
                     type: String,
                     desc: 'Tracking number of external shipment service',
                     allow_blank: false
            optional :data, type: JSON, desc: 'Params related to shipment service'
            requires :notification_url,
                     type: String,
                     desc: 'Shipment platform notification url',
                     allow_blank: false
          end
          post do
            {
              status: 'success',
              message: 'OK',
              code: 'OK',
              data: Business::Tracks.instance.add_shipment_track(params)
            }
          end

          route_param :track_id do
            desc 'Tracking shipment' do
              detail 'Tracking shipment'
              success model: Entities::Tracks, examples: Entities::Tracks.tracking_example
            end
            params do
              requires :track_id,
                       type: String,
                       desc: 'Track id of the shipment',
                       allow_blank: false
            end
            post do
              Business::Tracks.instance.track_shipment(params[:track_id])
              {
                status: 'success',
                message: 'We just start to track your shipment, wait for notifications',
                code: 'OK'
              }
            end
          end
        end
      end
    end
  end
end
