# frozen_string_literal: true

require_relative 'endpoints/tracks'

module Routes
  module V1
    # Entry point class for routes
    class API < Grape::API
      version 'v1'
      format :json

      get :health do
        { status: 'ok' }
      end

      mount Endpoints::Tracks

      # TODO: This is just for testing purposes, this code do not belong here, but in shipment service
      post :notification do
        Logger.new(STDOUT).info 'Notification received'
      end
    end
  end
end
