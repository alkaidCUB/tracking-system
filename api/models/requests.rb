# frozen_string_literal: true

require 'sequel'
require './api/tasks/services'

module Models
  # Request sequel model
  class Request < Sequel::Model(Services[:database][:requests])
    unrestrict_primary_key
  end
end
