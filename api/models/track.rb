# frozen_string_literal: true

require 'sequel'
require './api/tasks/services'

module Models
  # Track sequel model
  class Track < Sequel::Model(Services[:database][:tracks])
    unrestrict_primary_key
  end
end
