# frozen_string_literal: true

require './api/dao/track_dao'
require './api/utils/shipment_services_strategy/creator'
require './api/exceptions/invalid_track_id'

module Business
  class Tracks
    include Singleton

    def add_shipment_track(params)
      shipment_service = params[:shipment_service]

      ShipmentServicesStrategy::Creator.for(shipment_service).new(params[:data])

      track_id = generate_track_id

      track_dao.add(
        track_id,
        params[:shipment_service],
        params[:data],
        params[:notification_url]
      )

      Logger.new(STDOUT).info "New track added: track_id: #{track_id}, shipment_service: #{shipment_service}"

      {
        track_id: track_id
      }
    end

    def track_shipment(track_id)
      track = track_dao.search(track_id)
      raise Exceptions::InvalidTrackIdException if track.nil?

      shipment_service = track[:shipment_service]
      data = JSON.parse(track[:data], symbolize_names: true)

      data[:notification_url] = track[:notification_url]
      data[:track_id] = track_id

      strategy = ShipmentServicesStrategy::Creator.for(shipment_service).new(data)

      strategy.track_shipment

      Logger.new(STDOUT).info "Start tracking shipment: track_id: #{track_id}, "\
                      "shipment_service: #{shipment_service}"
    end

    private

    def generate_track_id
      Services[:uuid_generator].generate
    end

    def track_dao
      DAO::TrackDAO.instance
    end
  end
end
