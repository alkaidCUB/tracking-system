# frozen_string_literal: true

require 'dry-container'
require 'dry-auto_inject'
require 'sequel'
require './api/utils/id_generator'

require_relative 'service_config'
require 'fedex'

# Maintains the services in memory or creates them accordingly
class Services
  extend Dry::Container::Mixin

  register :database, memoize: true do
    configuration = ServiceConfig[:configuration]
    Sequel.connect(configuration['database']['endpoint'])
  end

  register :uuid_generator, memoize: true do
    UUIDGenerator.instance
  end

  register :fedex, memoize: true do
    configuration = ServiceConfig[:configuration][:fedex][:credentials]
    Fedex::Shipment.new(key: configuration[:key],
                        password: configuration[:password],
                        account_number: configuration[:account_number],
                        meter: configuration[:meter],
                        mode: configuration[:mode])
  end
end

ServicesContainer = Dry::AutoInject(Services)
