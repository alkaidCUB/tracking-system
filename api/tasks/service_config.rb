# frozen_string_literal: true

require 'dry-container'
require 'dry-auto_inject'
require 'config'

# Configuration container.
class ServiceConfig
  extend Dry::Container::Mixin

  register :configuration, memoize: true do
    Config.load_and_set_settings('./api/config/development.yaml')
  end
end

ConfigContainer = Dry::AutoInject(ServiceConfig)
