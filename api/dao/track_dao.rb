# frozen_string_literal: true

require './api/models/track'

module DAO
  # DAO Class to manage tracks
  class TrackDAO
    include Singleton

    def add(id_track, shipment_service, data, notification_url)
      model.insert(id_track: id_track,
                   shipment_service: shipment_service,
                   data: data.to_json,
                   notification_url: notification_url)
    end

    def search(id_track)
      model.find(id_track: id_track)
    end

    private

    def model
      Models::Track
    end
  end
end
