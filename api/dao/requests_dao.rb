# frozen_string_literal: true

require './api/models/requests'

module DAO
  # DAO Class to manage requests
  class RequestsDAO
    include Singleton

    def add(track_id, status, data = nil)
      model.insert(track_id: track_id,
                   status: status,
                   data: data)
    end

    private

    def model
      Models::Request
    end
  end
end
