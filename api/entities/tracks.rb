# frozen_string_literal: true

require 'grape-entity'

module Entities
  # Response class entity for charges
  class Tracks < Grape::Entity
    expose :track_id, documentation: { type: 'String', desc: 'Tracking system id' }

    def self.add_tracks_example
      {
        'application/json' =>
        {
          status: 'success',
          data: {
            'track_id': '1a8ab889a99999'
          },
          message: 'ok',
          code: 'OK'
        }
      }
    end

    def self.tracking_example
      {
        'application/json' =>
            {
              status: 'success',
              message: 'ok',
              code: 'OK'
            }
      }
    end
  end
end
