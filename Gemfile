# frozen_string_literal: true

source 'http://rubygems.org' do
  gem 'config',                 '~> 2.2'
  gem 'dry-auto_inject',        '~> 0.4'
  gem 'dry-container',          '~> 0.6'
  gem 'fedex',                  '~> 3.10.11'
  gem 'grape',                  '~> 1.2'
  gem 'grape-entity'
  gem 'httparty',               '~> 0.18.1'
  gem 'json',                   '~> 2.2'
  gem 'pg',                     '~> 1.1'
  gem 'puma',                   '~> 4.0'
  gem 'rake',                   '~> 12.3'
  gem 'sequel',                 '~> 5.35'
  gem 'sequel_postgresql_triggers', '~> 1.5'
  gem 'uuidtools', '~> 2.2'

  group :development, :test do
    gem 'byebug'
    gem 'pry'
    gem 'rb-readline'
  end

  group :development do
    gem 'rubocop'
  end

  group :test do
    gem 'colorize',             '~> 0.8'
    gem 'faker',                '~> 1.8'
    gem 'minitest',             '~> 5.11'
    gem 'minitest-reporters',   '~> 1.3'
    gem 'mocha',                '1.5.0'
    gem 'rack-test',            '~> 1.1'
    gem 'simplecov', require: false
    gem 'webmock', '~> 3.0'
  end
end
